/**
 * Created by richardmaglaya on 2017-10-17.
 */
'use strict';

var Validator = require('jsonschema').Validator;


var _validateSchema = function(data, schemas) {
	var validator = new Validator();
	
	//-- Add reference schemas
	if (schemas.length > 1) {
		for (var i = 1; i < schemas.length; i++) {
			validator.addSchema(schemas[i]);
		}
	}
	
	var attrName, len, item;
	var vres = validator.validate(data, schemas[0]);
	if (vres.errors.length > 0) {
		if (vres.errors[0].property === 'instance') {
			attrName =vres.errors[0].message;
			len = 'Unsupported attribute: '.length;
			for (item =1; item< vres.errors.length; item++){
				attrName = attrName + ',' + vres.errors[item].message.substring(len);
			}
			
			return {errorCode: 'REQUEST_INVALID_FORMAT', errorField: attrName };
		} else {
			//-- Get the attribute name
			attrName = vres.errors[0].property;
			var pos = attrName.indexOf('instance.');
			if (pos === 0) {
				attrName = attrName.substring('instance.'.length);
			}
			//-- Send response
			if (vres.errors[0].message === 'is required') {
				return {errorCode: 'DATA_DO_NOT_EXIST', errorField: attrName };
			} else if (vres.errors[0].message.indexOf('Unsupported attribute') === 0){
				len = 'Unsupported attribute: '.length;
				attrName = 'Unsupported attribute:'+attrName+':'+vres.errors[0].message.substring(len);
				for (item =1; item< vres.errors.length; item++) {
					attrName=attrName+','+vres.errors[item].property.substring('instance.'.length)+':'+vres.errors[item].message.substring(len);
				}
				return {errorCode: 'REQUEST_INVALID_FORMAT', errorField: attrName };
			}else {
				return {errorCode: 'DATA_INVALID', errorField: attrName };
			}
		}
	} else {
		return null;
	}
};


var ValidateRequestBody = function(data, schema, res) {
	
	console.log('%j', { function: 'Schema-Validator.ValidateRequestBody received arguments', schema: schema });
	
	var result = _validateSchema(data, schema);
	
	if (result === null) {
		return true;
	} else {
		console.log('%j', { function: 'Schema-Validator.ValidateRequestBody result', result: result });
		res.writeHead(400, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(result));
		return false;
	}
};


var ValidateRequestBodyWithOutRes = function(data, schema, callback) {
	var result = _validateSchema(data, schema);
	
	if (result === null) {
		callback(null, true);
	} else {
		callback(result.errorField, false);
		
	}
};


module.exports = function init(config, logger) {
	var _this = exports;
	_this.config = config;
	_this.logger = logger;
	_this.validateRequestBody = ValidateRequestBody;
	_this.validateRequestBodyWithOutRes = ValidateRequestBodyWithOutRes;
	return _this;
};