/**
 * Created by richardmaglaya on 2017-10-15.
 */
'use strict';

var loggerName = 'pdr';
var log4js = require('log4js');

log4js.configure({
    appenders: [
        {
            'type': 'console',
            'layout': {
                'type': 'pattern',
                'pattern': '%[[%d%d{O}] Severity=\'%p\'%] %m'
            }
        },
        {
            'type': 'file',
            'absolute': true,
            'filename': './logs/pdr.log',
            'maxLogSize': 20485760,
            'backups': 10,
            'category': loggerName,
            'layout': {
                'type': 'pattern',
                'pattern': '[%d%d{O}] Severity=\'%p\' %m'
            }
        }
    ]
});

var logger = log4js.getLogger(loggerName);
logger.setLevel('INFO');

Object.defineProperty(exports, 'LOG', {value: logger});

