/**
 * Created by richardmaglaya on 2017-10-15.
 */
'use strict';

var path = require('path');
var log4js = require('log4js');
var fs = require('fs');

var entryPoint = process.argv[1];
var formatted = 'EntryPoint="%s" Module="%s" Data=';
var useDefaultConsole = process.env.NODE_ENV === 'test' || false;
var configurationFile = null;
var configurationData = null;
var configLastUpdated = new Date();


//-- Private functions
function updateConfigurationFromFile() {
    fs.readFile(configurationFile, 'utf-8', function read(err, data) {
        if (err) {
            console.log(err);
        }
        else {
            var appDir = path.dirname(process.argv[1]);
            var jData = JSON.parse(data);
            for (var i = 0; i < jData.appenders.length; i++) {
                if (jData.appenders[i].filename) {
                    jData.appenders[i].filename = path.join(appDir, jData.appenders[i].filename);
                }
            }
            configurationData = {
                updated: new Date(),
                data: JSON.stringify(jData)
            };
            console.log('Logger Configuration Updated From Disk');
        }
    });
}


//-- Exposed functions
var Initialize = function (category) {
    if (configurationFile) {
        if (!useDefaultConsole) {
            log4js.replaceConsole();
        }
        var logger = log4js.getLogger(category);
        return {
            name: category,
            trace: function () {
                logger.trace(formatted, entryPoint, this.name, JSON.stringify(arguments));
            },
            debug: function () {
                logger.debug(formatted, entryPoint, this.name, JSON.stringify(arguments));
            },
            info: function () {
                logger.info(formatted, entryPoint, this.name, JSON.stringify(arguments));
            },
            warn: function () {
                logger.warn(formatted, entryPoint, this.name, JSON.stringify(arguments));
            },
            error: function () {
                if(arguments[0].res){
                    var errorCode = arguments[0].errorCode;
                    if(!errorCode){
                        errorCode = 'SERVER ERROR'
                    }
                    var errormatted = {
                        module: this.name,
                        api_url: arguments[0].res.req.route.path, //-- "/v1/users/{userId}/getuserinfo",
                        params: arguments[0].res.req.params,
                        response: '',
                        error_code: errorCode,
                        error: arguments[0].message,
                        time_stamp: new Date().toISOString()
                    }
                    logger.error(errormatted);
                }else{
                    logger.error(formatted, entryPoint, this.name, JSON.stringify(arguments));
                }
                
            },
            fatal: function () {
                logger.fatal(formatted, entryPoint, this.name, JSON.stringify(arguments));
            },
            log: function () {
                console.log('[%s]:', this.name, arguments);
            }
        };
    }
    else {
        throw new Error('invalid_log4js_configuration_file_path');
    }
};


module.exports = function init(configurationFilePath) {
    configurationFile = configurationFilePath;
    updateConfigurationFromFile();
    fs.watchFile(configurationFilePath, function (curr, prev) {
        if (curr.mtime > prev.mtime) {
            updateConfigurationFromFile();
        }
    });
    setInterval(function () {
        if (configurationData) {
            if (configurationData.updated > configLastUpdated) {
                configLastUpdated = configurationData.updated;
                var config = JSON.parse(configurationData.data);
                log4js.configure(config);
                if (!useDefaultConsole) {
                    log4js.replaceConsole();
                }
            }
        }
    }, 10000);
    return {
        initialize: Initialize
    };
};
