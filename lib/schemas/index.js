/**
 * Created by richardmaglaya on 2017-10-20.
 */
'use strict';

var _ = require('underscore');


module.exports = function () {
    return _.extend(
		require('./enum/enums')(),
        require('./oauth/permissions-schema')(),
		require('./user/users-schema')()
    );
};