/**
 * Created by richardmaglaya on 2017-11-2.
 */
'use strict';


var GetUserByIdResponse = {
	'id': '/GetUserByIdResponse',
	'type': 'object',
	'properties': {
		'user_id':		{ type: 'string' },
		'full_name':		{ type: 'string' },
		'email':		{ type: 'string' }
	}
};


var GetUsersResponse = {
	'id': '/GetUsersResponse',
	'type': 'object',
	'properties': {
		'user_id':		{ type: 'string' },
		'full_name':		{ type: 'string' },
		'email':		{ type: 'string' }
	}
};


var NewUserSchema = {
	'id': '/UsersSchema',
	'type': 'object',
	'properties': {
		'user_id': 	{ type: 'string' },
		'email': 	{ type: 'string' },
		'full_name': { type: 'string' }
	}
};


var UpdateUserSchema = {
	'id': '/UsersSchema',
	'type': 'object',
	'properties': {
		'user_id': 	{ type: 'string' },
		'email': 	{ type: 'string' },
		'full_name': { type: 'string' }
	}
};


module.exports = function() {
	return {
		GetUserByIdResponse: GetUserByIdResponse,
		GetUsersResponse: GetUsersResponse,
		NewUserSchema: NewUserSchema,
		UpdateUserSchema: UpdateUserSchema
	};
};