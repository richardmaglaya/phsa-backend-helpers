/**
 * Created by richardmaglaya on 2017-10-23.
 */
'use strict';


var DBName = {
	USERS: 'Users',
	PERMISSIONS: 'Permissions',
	
};


var HttpErrorCodes = {
	DATA_DO_NOT_EXIST: 		{STATUS:400, ERROR_MESSAGE_EN:'user account not exists'},
	PASSWORD_INCORRECT:		{STATUS:400, ERROR_MESSAGE_EN:'password error'},
	ACCOUNT_INACTIVE:		{STATUS:401, ERROR_MESSAGE_EN:'user status is inactive'},
	NOT_EMPLOYEE:			{STATUS:403, ERROR_MESSAGE_EN:'user is not a employee'},
	EMAIL_ALREADY_IN_USE:	{STATUS:409, ERROR_MESSAGE_EN:'email is already registered'}
};


var HttpStatusCodes = {
	_200: 200,
	_404: 404,
	_500: 500,
	
};


module.exports = function () {
	return {

		//-- NOTE to Developers (2015-05-13): 1) Let's be consistent that all exposed JSON objects should start with Upper-Case
		//-- NOTE to Developers (2015-05-13): 2) For READABILITY, please help to arrange these exposed functions in alphabetical order
		//-- NOTE to Developers (2015-05-13): 3) For READABILITY, please help to maintain double-spaces between functions declarations
		
		DBName: DBName,
		HttpErrorCodes: HttpErrorCodes,
		HttpStatusCodes: HttpStatusCodes

		//-- NOTE to Developers (2015-05-13): 1) Let's be consistent that all exposed JSON objects should start with Upper-Case
		//-- NOTE to Developers (2015-05-13): 2) For READABILITY, please help to arrange these exposed functions in alphabetical order
		//-- NOTE to Developers (2015-05-13): 3) For READABILITY, please help to maintain double-spaces between functions declarations
		
	};
}