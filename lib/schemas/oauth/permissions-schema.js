/**
 * Created by richardmaglaya on 2017-10-20.
 */
'use strict';


var ResourceSchema = {
	'id': '/ResourceSchema',
	'type': 'object',
	'properties': {
		'user_add': 	{ type: 'string' },
		'user_view': 	{ type: 'string' },
		'user_delete': 	{ type: 'string' },
		'user_update': 	{ type: 'string' }
	}
};


var RoleSchema = {
	'id': '/RoleSchema',
	'type': 'object',
	'properties': {
		'institution_id': 	{ type: 'string' },
		'institution_name': { type: 'string' },
		'role_id': 			{ type: 'string' },
		'role_name': 		{ type: 'string' },
		'resources':		{ type: 'object',  properties: {'$ref': 'ResourceSchema'} },
		'effective_date':	{ type: 'string' },
		'expiry_date':		{ type: 'string' },
	}
};


var PermissionsSchema = {
    'id': '/PermissionsSchema',
    'type': 'object',
    'properties': {
        'permission_id': { type: 'string' },
        'roles': {
            type: 		'array',
            items: {
                '$ref': 'RoleSchema' }}
        //-- Add more Permissions properties here
    }
};


module.exports = function() {
    return {
        PermissionsSchema: PermissionsSchema
    };
};