'use strict';

var util = require('util');
var enums = require('../schemas/enum/enums')();


function HttpException(status, message) {
	this.status = status;
	this.message = message;
	
	this.getStatus = function() { return this.status; };
	this.getMessage = function() { return this.message; };
}


function HttpErrorCodeException(status, errorCode, fieldName, fieldValue, messages) {
	this.errorCode = errorCode;
	this.fieldName = fieldName;
	this.fieldValue = fieldValue;
	
	//-- create the http response body
	var message;
	if (typeof fieldValue !== 'undefined') {
		message = {errorCode: errorCode, fieldName: fieldName, fieldValue: fieldValue};
	} else if (typeof fieldName !== 'undefined') {
		message = {errorCode: errorCode, fieldName: fieldName};
	} else {
		message = {errorCode: errorCode};
	}
	message.messages = messages;
	
	HttpException.call(this, status, message);
	
	this.getStatus = function() { return this.status; };
	this.getFieldName = function() { return this.fieldName; };
	this.getFieldValue = function() { return this.fieldValue; };
}
util.inherits(HttpErrorCodeException, HttpException);


function CommonHttpErrorException(errorcode, fieldName, fieldValue) {
	var httpErrorCodes = enums.HttpErrorCodes;
	
	if (httpErrorCodes[errorcode]) {
		var messages = {
			messageCode:errorcode,
			en: httpErrorCodes[errorcode].ERROR_MESSAGE_EN
		};
		if (httpErrorCodes[errorcode].STATUS === 400){
			HttpErrorCodeException.call(this, httpErrorCodes[errorcode].STATUS, 'INVALID_PARAMETER', fieldName, fieldValue, messages);
		} else if (httpErrorCodes[errorcode].STATUS === 401){
			HttpErrorCodeException.call(this, httpErrorCodes[errorcode].STATUS, 'PERMISSION_NOT_ALLOWED', fieldName, fieldValue, messages);
		} else if (httpErrorCodes[errorcode].STATUS === 403){
			HttpErrorCodeException.call(this, httpErrorCodes[errorcode].STATUS, 'OPERATION_NOT_PERMISSION', fieldName, fieldValue, messages);
		} else if (httpErrorCodes[errorcode].STATUS === 404){
			HttpErrorCodeException.call(this, httpErrorCodes[errorcode].STATUS, 'DATA_DOES_NOT_EXIST', fieldName, fieldValue, messages);
		} else if (httpErrorCodes[errorcode].STATUS === 409){
			HttpErrorCodeException.call(this, httpErrorCodes[errorcode].STATUS, 'DATA_CONFLICT', fieldName, fieldValue, messages);
		} else if (httpErrorCodes[errorcode].STATUS === 500){
			HttpErrorCodeException.call(this, httpErrorCodes[errorcode].STATUS, 'SERVER_ERROR', fieldName, fieldValue, messages);
		} else {
			HttpErrorCodeException.call(this, 500, 'SERVER_ERROR', 'error STATUS not exists', httpErrorCodes[errorcode].STATUS , messages);
		}
	} else {
		HttpErrorCodeException.call(this, 500, 'SERVER_ERROR', 'error code not exists', errorcode);
	}
}
util.inherits(CommonHttpErrorException, HttpErrorCodeException);


function InvalidParameterException(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 400, 'INVALID_PARAMETER', fieldName, fieldValue, messages);
}
util.inherits(InvalidParameterException, HttpErrorCodeException);


function AccountNotExistsException(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 400, 'ACCOUNT_NOT_EXISTS', fieldName, fieldValue, messages);
}
util.inherits(AccountNotExistsException, HttpErrorCodeException);


function PasswordErrorException(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 400, 'PASSWORD_ERROR', fieldName, fieldValue, messages);
}
util.inherits(PasswordErrorException, HttpErrorCodeException);


function PermissionNotAllowedException(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 401, 'PERMISSION_NOT_ALLOWED', fieldName, fieldValue, messages);
}
util.inherits(PermissionNotAllowedException, HttpErrorCodeException);


function AccountInvalidException(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 401, 'ACCOUNT_INVALID', fieldName, fieldValue, messages);
}
util.inherits(AccountInvalidException, HttpErrorCodeException);


function OperationNotPermissionException(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 403, 'OPERATION_NOT_PERMISSION', fieldName, fieldValue, messages);
}
util.inherits(OperationNotPermissionException, HttpErrorCodeException);


function ResourceNotFoundException(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 404, 'DATA_NOT_EXIST', fieldName, fieldValue, messages);
}
util.inherits(ResourceNotFoundException, HttpErrorCodeException);


function DataConflictException(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 409, 'DATA_CONFLICT', fieldName, fieldValue, messages);
}
util.inherits(DataConflictException, HttpErrorCodeException);


function EmailAlreadyInUseException(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 409, 'EMAIL_ALREADY_IN_USE', fieldName, fieldValue, messages);
}
util.inherits(EmailAlreadyInUseException, HttpErrorCodeException);


function ErrorInServer(fieldName, fieldValue, messages) {
	HttpErrorCodeException.call(this, 500, "SERVER_ERROR", fieldName, fieldValue, messages);
}
util.inherits(ErrorInServer, HttpErrorCodeException);


module.exports.InvalidParameterException = InvalidParameterException;
module.exports.PermissionNotAllowedException = PermissionNotAllowedException;
module.exports.AccountNotExistsException = AccountNotExistsException;
module.exports.PasswordErrorException = PasswordErrorException;
module.exports.ResourceNotFoundException = ResourceNotFoundException;
module.exports.AccountInvalidException =AccountInvalidException;
module.exports.DataConflictException = DataConflictException;
module.exports.EmailAlreadyInUseException = EmailAlreadyInUseException;
module.exports.OperationNotPermissionException = OperationNotPermissionException;
module.exports.CommonHttpErrorException = CommonHttpErrorException ;
module.exports.ErrorInServer = ErrorInServer;