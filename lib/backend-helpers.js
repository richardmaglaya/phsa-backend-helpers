/**
 * Created by richardmaglaya on 2017-10-11.
 */
'use strict';

var thisFilePath = module.filename.split('/');
var thisFilename = thisFilePath[thisFilePath.length-1];
var mssql = require('mssql');


//-- Exposed functions
var MsSqlClientHelper = function (mssqlConfig) {
	var _this = exports;
	var conn = new mssql.ConnectionPool(mssqlConfig, err => {
		_this.logger.log('err %j', err);
	})
    
    return conn;
};


var ConfigHelper = function () {
    return require('./helpers/config-helper')();
};


var DataGenerationHelper = function () {
    return require('./helpers/data-generation-helper')();
};


var Enums = function () {
	return require('./schemas/enum/enums')();
};


var EnvironmentHelper = function () {
    return require('./helpers/environment-helper')();
};


var HttpHelper = function () {
    return require('./helpers/http-helper')();
};


var JsonHelper = function() {
    return require('./helpers/json-helper')();
};


var Loggers = function () {
    var Log4jsHelper = function (level, location, category) {
        return require('./loggers/log4js-helper')(level, location, category);
    };
    return {
        log4jsHelper: Log4jsHelper
    };
};


var ParameterHelper = function () {
    return require('./helpers/parameter-helper')();
};


var ResponseUtils = function() {
    return require('./helpers/response-utils.js')();
};


var Schemas = function() {
    return require('./schemas')();
};


var SchemaValidator = function() {
    require('./validators/schema-validator')();
};


var Validators = function () {
    var DataTypeValidator = function () {
        return require('./validators/data-type-validator')();
    };
    var DateValidator = function () {
        return require('./validators/date-validator')();
    };
    return {
        dataTypeValidator: DataTypeValidator,
        dateValidator: DateValidator
    };
};


module.exports = function(config, msSqlConfig, logger) {
    var _this = exports;
    _this.config = config || {};
    _this.logger = logger || console;
    _this.loggers = Loggers;
    _this.configHelper = ConfigHelper;
    _this.dataGenerationHelper  = DataGenerationHelper;
	_this.enums                 = require('./schemas/enum/enums')();
    _this.environmentHelper     = EnvironmentHelper;
    _this.httpHelper            = HttpHelper;
    _this.jsonHelper            = JsonHelper;
    
    _this.parameterHelper       = ParameterHelper;
    _this.responseUtils         = ResponseUtils;
    _this.schemas               = require('./schemas');
    _this.schemaValidator       = SchemaValidator;
    _this.validators            = Validators;
    _this.config                = config;
    _this.msSqlConfig           = msSqlConfig;
    
    //-- Exposed functions
    _this.mssqlClientHelper = MsSqlClientHelper;
    _this.httpExceptions    = require('./exceptions/http-exceptions');
    _this.oauthHelper       = require('./helpers/oauth-helper')(_this.config, _this.logger);
    
    //-- _this.base64            = require('./helpers/base64')();
    //-- _this.sha1              = require('./helpers/sha1')();
    
    _this.logger.log('Initialized PHSA-Backend-Helper');
    
    return _this;
};
