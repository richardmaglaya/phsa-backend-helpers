/**
 * Created by richardmaglaya on 2017-10-26.
 */
'use strict';

var async = require('async');
var crypto = require('crypto');
var https = require('https');


var _getPropertyNameAndValue = function(data) {
	var result = [];
	if (data && data.length>0) {
		for (var propName in data) {
			var val = data[propName];
			if (typeof val === 'object') {
				if (!(val instanceof Array)) {
					_getPropertyNameAndValueRecursive(val, propName, result);
				} else {
					result.push({name: propName, value: val});
				}
			} else {
				result.push({name: propName, value: val});
			}
		}
	}
	
	return result;
};


var _getPropertyNameAndValueRecursive = function (data, parent, result) {
	if (data && data.length>0) {
		for (var propName in data) {
			var val = data[propName];
			if (typeof val === 'object') {
				if (!(val instanceof Array)) {
					_getPropertyNameAndValueRecursive(val, parent + '.' + propName, result);
				} else {
					result.push({name: propName, value: val});
				}
			} else {
				result.push({name: parent + '.' + propName, value: val});
			}
		}
	}
	
}


var CheckPermission = function(req, params, permission, action, res, callback) {
	var _this = exports;
	
	async.waterfall([
		function(nextstep){
			_this.getPermissions(req, function(error, permissions){
				
				_this.logger.info('%j', { function: 'DEBUG-INFO: OAuth-Utils.checkPermission permissions: ', permissions: permissions });
				
				if (error) {
					res.writeHead(401, {'Content-Type': 'application/json'});
					res.end(JSON.stringify({errorCode: 'PERMISSION_NOT_ALLOWED', errorField: 'INVALID_TOKEN'}));
					return false;
				}
				else {
					if(permission === undefined || permission === '' || permission === null){
						callback(null,true);
					}else{
						nextstep(null, permissions);
					}
					
				}
			})
		},
		function(token, nextstep){
			if (typeof params.loginUserId !== 'undefined' && typeof token.userId !== 'undefined') {
				if (token.userId !== params.loginUserId) {
					return false;
				}
			}
			if (typeof params.userId !== 'undefined') {
				permission = permission.replace("{userId}", params.userId);
			}
			if (typeof params.divisionId !== 'undefined') {
				permission = permission.replace("{divisionId}", params.divisionId);
			}
			if (typeof params.divisionGroupId !== 'undefined') {
				permission = permission.replace("{divisionGroupId}", params.divisionGroupId);
			}
			var permItems = permission.split(":");
			var flag;
			for (var attrName in token.permissions) {
				var hasPermItems = attrName.split(":");
				for (var i = 0; i < hasPermItems.length; i++) {
					console.log('hasPermItems＄＄＄＄'+hasPermItems[i]);
					console.log('hasPermItems＄＄＄＄'+permItems[i]);
					console.log('token.permissions[attrName]＄＄＄＄'+token.permissions[attrName]);
					console.log('action＄＄＄＄'+action);
					if (i >= permItems.length) {
						flag = false;
						break;
					}
					if (hasPermItems[i] !== '*') {
						if (permItems[i] !== hasPermItems[i]) {
							flag = false;
							break;
						}
					}
					flag = true;
				}
				if (flag && (token.permissions[attrName] & action) > 0) {
					flag = true;
					break;
				}else{
					flag = false;
				}
			}
			if(flag){
				nextstep(null,true)
//                return true;
			}else{
				res.writeHead(401, {'Content-Type': 'application/json'});
				res.end(JSON.stringify({errorCode: 'PERMISSION_NOT_ALLOWED', errorField: 'INVALID_TOKEN'}));
				return false;
			}
		}
	],function(error,result){
		callback(error,result);
	})
	
};


var Flatten = function(data) {
	console.log(data);
	var propNameValues = _getPropertyNameAndValue(data);
	var result = {};
	for (var i = 0; i < propNameValues.length; i++) {
		result[propNameValues[i].name] = propNameValues[i].value;
	}
	console.log(result);
	return result;
};


var GenerateToken = function() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
	});
};


var GetPermissions = function(req, callback){
	var _this = exports;
	var headerToken = req.headers.authorization;
	if (headerToken) {
		var matches = headerToken.match(/[bB]earer\s(\S+)/);
		headerToken = matches[1];
	}
	//-- BUG: check the status code; only allow if '200'
	console.log('_this.backendHelpers.host,' + _this.host);
	
	var options = {
		host: _this.host,
		port: _this.port,
		path: '/oauth/tokendetail?accessToken='+headerToken,
		method: 'GET',
		headers:{
			'content-type': "application/json"
		}
	};
	var req = https.request(options, function (res) {
		res.setEncoding('utf8');
		res.on('data',function(data){
			var dataObj = JSON.parse(data)
			if (dataObj.errorCode !== undefined && dataObj.errorCode !== null && dataObj.errorCode !== '') {
				//-- callback(new _this.httpExceptions.PermissionNotAllowedException("INVALID_TOKEN",dataObj.fieldName,dataObj.fieldValue))
			} else {
				//-- callback(null,dataObj);
			}
			callback(null, dataObj);
		})
	});
	req.end();
};


var OauthCheckPermission = function(token, params, permission, action) {
	if (typeof params.loginUserId !== 'undefined' && typeof token.userId !== 'undefined') {
		if (token.userId !== params.loginUserId) {
			return false;
		}
	}
	if (typeof params.userId !== 'undefined') {
		permission = permission.replace('{userId}', params.userId);
	}
	
	var permItems = permission.split(":");
	if (token && token.permissions && token.permissions.length>0) {
		for (var attrName in token.permissions) {
			var hasPermItems = attrName.split(":");
			for (var i = 0; i < hasPermItems.length; i++) {
				if (i >= permItems.length) {
					return false;
				}
				if (hasPermItems[i] !== '*') {
					if (permItems[i] !== hasPermItems[i]) {
						return false;
					}
				}
			}
			if (token.permissions[attrName] & action > 0) {
				return true;
			}
		}
	}
	
	return false;
};


var Sha1hash = function(data, salt) {
	var shasum = crypto.createHash('sha1');
	if (typeof salt !== 'undefined' && salt !== null) {
		shasum.update(data + salt);
	} else {
		shasum.update(data);
	}
	return shasum.digest('hex');
};


module.exports = function init(config, logger) {
	var _this = exports;
	_this.config = config;
	_this.logger = logger;
	_this.sha1hash = Sha1hash;
	_this.flatten = Flatten;
	
	_this.httpExceptions = require('../exceptions/http-exceptions');
	
	_this.generateToken = GenerateToken;
	_this.getPermissions = GetPermissions;
	_this.checkPermission = CheckPermission;
	_this.oauthCheckPermission = OauthCheckPermission;
	_this.host = 'localhost';
	_this.port = '8002';
	_this.ActionType = {CREATE: 1, READ: 2, UPDATE: 4, DELETE: 8};
	return _this;
};