/**
 * Created by richardmaglaya on 2017-10-13.
 */
'use strict';

var thisFilePath = module.filename.split('/');
var thisFilename = thisFilePath[thisFilePath.length-1];


//-- Exposed functions
var ResolveRawConfig = function (rawConfig) {
	var _this = exports;
	console.log('==============================================================');
	console.log('\t\tDEBUG-INFO: ' + thisFilename + ' ResolveRawConfig()');
	console.log('\t\t            getEnvironmentValue (rawConfig) %j', rawConfig);
	console.log('\t\t            getEnvironmentValue (rawConfig.mssql) %j', rawConfig.mssql);
	
	var mssqlConfig = JSON.stringify(rawConfig.mssql);
	var resolvedRawConfig = {
		mssql: {
			//-- getEnvironmentValue params => mssqlConfig, env, key, defaultValue, callback
			hosts: _this.environmentHelper.getEnvironmentValue(mssqlConfig, null, 'hosts', null),
			database: _this.environmentHelper.getEnvironmentValue(mssqlConfig, null, 'database', null),
			replSet: _this.environmentHelper.getEnvironmentValue(mssqlConfig, null, 'replSet', null)
		},
		rawConfig: rawConfig
	};
	return resolvedRawConfig;
};


var ResolveUploadConfig = function (rawConfig) {
	var _this = exports;
	console.log('==============================================================');
	console.log('\t\tDEBUG-INFO: ' + thisFilename + ' ResolveUploadConfig()');
	//console.log('\t\t            getEnvironmentValue (rawConfig) %j', rawConfig);
	console.log('\t\t            getEnvironmentValue (rawConfig.config) %j', rawConfig.config);
	var resolvedRawConfig = {
		serverConfig: {
			shared:rawConfig.config.shared,
			bucket: _this.environmentHelper.getEnvironmentValue(JSON.stringify(rawConfig.config), null, 'bucket', null)
		},
		rawConfig: rawConfig
	};
	return resolvedRawConfig;
};

module.exports = function init() {
	var _this = exports;
	_this.environmentHelper = require('../backend-helpers')().environmentHelper();
	_this.resolveRawConfig = ResolveRawConfig;
	_this.resolveUploadConfig=ResolveUploadConfig;
	return _this;
};
