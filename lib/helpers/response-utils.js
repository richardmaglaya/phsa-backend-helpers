/**
 * Created by richardmaglaya on 2017-10-11.
 */
'use strict';


var ProcessResponse = function(error, apiResult, response, acceptEncoding, accept) {
	
	var logCtx={};
	if (error) {
		if (error instanceof Error) {
			logCtx.error = error;
			responseServerError(500, 'Server is unable to process request', logCtx, response);
		} else {
			writeJsonResponse(error.getStatus(), error.getMessage(), logCtx, response);
		}
	} else {
		if (apiResult) {
			if (apiResult.data) {
				if (acceptEncoding && acceptEncoding.toLowerCase().indexOf('gzip') >= 0 ) {
					writeGzipResponse(apiResult.status, apiResult.data, logCtx, response);
				}else {
					if (accept && accept.toLowerCase().indexOf('text/plain') >= 0) {
						writeTextResponse(apiResult.status, apiResult.data, logCtx, response);
					} else {
						writeJsonResponse(apiResult.status, apiResult.data, logCtx, response);
					}
				}
			} else {
				writeNoResponse(apiResult.status, logCtx, response);
			}
		}
	}
};


var responseServerError = function(errorCode, errorMessage, logCtx, response) {
	// Write to log
	logCtx.response = {errorCode: errorCode, errorMessage: errorMessage};
	logCtx.httpStatus = 500;
	console.error(logCtx);
	console.error('%j', {
		function: 'response-utils.responseServerError',
		logCtx: logCtx
	})
	
	//-- send response
	try {
		response.writeHead(errorCode, {'Content-Type': 'application/json'});
		response.end(JSON.stringify({errorCode: errorCode, errorMessage: errorMessage}));
	} catch (ex) {
		console.log(ex);
		response.end();
	}
};


var ResponseServerError = function(errorCode, errorMessage, logCtx, response) {
	responseServerError(errorCode, errorMessage, logCtx, response);
};


var logResponse = function(status, data, logCtx) {
	logCtx.httpStatus = status;
	if (data !== null) {
		logCtx.response = data;
	}
	
	if (status % 100 === 2) {
		//-- null;
	} else if (status % 100 === 4) {
		//-- null;
	} else if (status % 100 === 5) {
		console.error(logCtx);
	} else {
		//-- null;
	}
}


var writeJsonResponse = function(status, data, logCtx, response) {
	logResponse(status, data, logCtx);
	//-- send response
	try {
		response.writeHead(status, {'Content-Type': 'application/json'});
		response.end(JSON.stringify(data));
	} catch (ex) {
		console.log(ex);
		response.end();
	}
};


var writeGzipResponse = function(status, data, logCtx, response) {
	logResponse(status, data, logCtx);
	//-- send response
	try {
		response.writeHead(status, {'Content-Type': 'application/json', 'Content-Encoding': 'gzip'});
		var rs = new Readable();
		rs.push(JSON.stringify(data));
		rs.push(null);
		rs.pipe(zlib.createGzip()).pipe(response);
	} catch (ex) {
		console.log(ex);
		response.end();
	}
}


var WriteGzipResponse = function(status, data, logCtx, response) {
	writeGzipResponse(status, data, logCtx, response);
}


var WriteJsonResponse = function(status, data, logCtx, response) {
	writeJsonResponse(status, data, logCtx, response);
};


var writeTextResponse = function(status, data, logCtx, response) {
	logResponse(status, data, logCtx);
	// send response
	try {
		response.writeHead(status, {'Content-Type': 'text/plain'});
		response.end(JSON.stringify(data));
	} catch (ex) {
		console.log(ex);
		response.end();
	}
};


var WriteTextResponse = function(status, data, logCtx, response) {
	writeTextResponse(status, data, logCtx, response);
};


var writeNoResponse = function(status, logCtx, response) {
	logResponse(status, null, logCtx);
	//-- send response
	try {
		response.writeHead(status, {'Content-Type': 'text/plain'});
	} catch (ex) {
		console.log(ex);
	}
	response.end();
};


var WriteNoResponse = function(status, logCtx, response) {
	writeNoResponse(status, logCtx, response);
};


module.exports = function init() {
	var _this = exports;
	
	_this.processResponse 		= ProcessResponse;
	_this.responseServerError 	= ResponseServerError;
	_this.writeGzipResponse 	= WriteGzipResponse;
	_this.writeJsonResponse 	= WriteJsonResponse;
	_this.writeNoResponse 		= WriteNoResponse;
	_this.writeTextResponse 	= WriteTextResponse;
	
	return _this;
};

