/**
 * Created by richardmaglaya on 2017-10-13.
 */
'use strict';

var thisFilePath = module.filename.split('/');
var thisFilename = thisFilePath[thisFilePath.length-1];


var getEnvironmentValue = function(mssqlConfig, env, key, defaultValue, callback) {
	var environment = (env) ? env : (process.env.NODE_ENV || 'dev');
	var data = JSON.parse(mssqlConfig);
	
	//-- The shared values
	var environments = data.environments;
	var shared = data.shared;
	
	//-- The environment specific values
	var matchingEnvironment = environments[environment];
	
	console.log('\n\t\t            getEnvironmentValue (data) %j', data);
	console.log('\t\t            getEnvironmentValue (environments) %j', environments);
	console.log('\t\t            getEnvironmentValue (shared) %j', shared);
	console.log('\t\t            getEnvironmentValue (matchingEnvironment) %j', matchingEnvironment);
	
	//-- If the matching environment does not exist just use shared values.
	if (!matchingEnvironment) {
		matchingEnvironment = {};
	}
	
	/**
	 * Add the shared values to the environment specific ones.
	 * Skip if the environment overrides the shared.
	 */
	if (shared && shared.length>0) {
		for (var sharedKey in shared) {
			console.log('\t\t            getEnvironmentValue (sharedKey) %j', sharedKey);
			if (!matchingEnvironment[sharedKey]) {
				matchingEnvironment[sharedKey] = shared[sharedKey];
			}
		}
	}
	
	if (!matchingEnvironment[key]) {
		if (callback) {
			callback(null, defaultValue);
		}
		else {
			return defaultValue;
		}
	}
	else {
		if (callback) {
			callback(null, matchingEnvironment[key]);
		}
		else {
			//-- Note: return the entire `matchingEnvironment` if other configuration need to be tested
			//-- Sample matchingEnvironment[key] = [{"server":"127.0.0.1","port":27017}]
			//-- Sample matchingEnvironment = {"database":"pdr_qa","hosts":[{"server":"127.0.0.1","port":27017}]}
			return matchingEnvironment[key];
		}
	}
};


//-- Exposed functions
var GetEnvironmentValue = function (mssqlConfig, env, key, defaultValue, callback) {
	console.log('==============================================================');
	console.log('\tDEBUG-INFO: ' + thisFilename);
	console.log('\t            GetEnvironmentValue (mssqlConfig) ', mssqlConfig);
	console.log('\t            GetEnvironmentValue (env) ', env);
	console.log('\t            GetEnvironmentValue (key) ', key);
	console.log('\t            GetEnvironmentValue (defaultValue) ', defaultValue);
	console.log('\t            GetEnvironmentValue (callback) ', callback);
	
	if (callback && typeof(callback) === 'function') {
		getEnvironmentValue(mssqlConfig, env, key, defaultValue, callback);
	}
	else {
		return getEnvironmentValue(mssqlConfig, env, key, defaultValue);
	}
};


module.exports = function init() {
	return {
		getEnvironmentValue: GetEnvironmentValue
	};
};