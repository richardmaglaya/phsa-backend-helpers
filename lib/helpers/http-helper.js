/**
 * Created by richardmaglaya on 2017-10-13.
 */
'use strict';


//-- Exposed functions
var SendFormattedResponse = function (err, result, code, res) {
	var defaultError = {
		__error: 'request_error',
		__errorData: null
	};
	var errorData = (err) ? {
		__error: err.message || 'request_error',
		__errorData: err.data || null } : defaultError;
	var responseData = (err) ? errorData : result;
	var responseCode = (err) ? (err.code || 400) : (code || 200);
	res.json(responseCode, responseData);
};


module.exports = function init() {
	return {
		sendFormattedResponse: SendFormattedResponse
	};
};