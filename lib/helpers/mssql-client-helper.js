/**
 * Created by richardmaglaya on 2017-10-15.
 */
'use strict';

var thisFilePath = module.filename.split('/');
var thisFilename = thisFilePath[thisFilePath.length-1];
var mssql = require('mssql');


var Connect = function (config, options, callback) {
    
    var _this = exports;
    var args = arguments;
    var connString = args[0];
    var callback = args[1];
    
    _this.pg.defaults.poolSize = 50;
    _this.pg.connect(connString, function(error, client, done) {
        
        if (error){
            console.log('\t\t\tConnection FAIL. Callback with error\t= %j', error);
            return callback(error, null);
        } else {
            callback(null, {client: client, done: done});
        }
        
    });
};


module.exports = function init() {
    return {
        connect: Connect,
        thisFilePath: thisFilePath
    };
};
