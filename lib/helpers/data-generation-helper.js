/**
 * Created by richardmaglaya on 2017-10-13.
 */
'use strict';

var nodeUuid = require('uuid');


var IsValidUUID = (id) => {
    var patrn = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
    if(patrn.test(id)){
        return true;
    } else {
        return false;
    }
};


var CheckEmail = (email) => {
    var patrn=/^([\.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;
    if (!patrn.exec(email)) {
        return false;
    }
    return true;
};


var GenerateUUID = function () {
    return nodeUuid.v4();
};


var GenerateValidUTCDate = (time) => {
    if (time) {
        return moment(time).utc().toDate();
    } else {
        return moment().utc().toDate();
    }
};


module.exports = function init() {
    return {
        generateUUID: GenerateUUID,
        isValidUUID: IsValidUUID,
        checkEmail: CheckEmail,
        generateValidUTCDate: GenerateValidUTCDate
    };
};