/**
 * Created by richardmaglaya on 2017-10-12.
 */
'use strict';

var user = {
    username: '',
    full_name: '',
    permissions: {
        dashboard: true
    }
};


module.exports = function () {
    return {
        user: user
    };
};