/**
 * Created by richardmaglaya on 2017-10-12.
 */
'use strict';

var API = {
    shared: {
        domain: 'api.pdr.phsa.ca'
    },
    environments: {
        dev: {
            url: 'dev.',
            port: '',
            database: ''
        },
        test: {
            url: 'qa.',
            port: '',
            database: ''
        },
        prod: {
            url: '',
            port: '',
            database: ''
        }
    }
};


module.exports = function () {
    return {
        api: API
    };
};