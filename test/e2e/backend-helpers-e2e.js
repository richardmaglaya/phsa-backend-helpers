/**
 * Created by richardmaglaya on 2017-10-12.
 */
'use strict';

process.env.NODE_ENV = 'dev';

var should = require('should');
var backendHelpers = require('../../lib/backend-helpers')();
var phsaFixtures = require('../fixtures/phsa-fixtures');
var configServerFixtures = require('../fixtures/config-server-fixtures.js')();
console.log('%j', configServerFixtures);

var obj;
var domain  = configServerFixtures.api.shared.domain,
    ipAdd   = configServerFixtures.api.environment[process.env.NODE_ENV].ip,
    portNo  = configServerFixtures.api.environment[process.env.NODE_ENV].port,
    dbName  = configServerFixtures.api.environment[process.env.NODE_ENV].database;

describe('Backend Helpers [End-to-End]', function () {
    
    //-- console.log('DEBUG-INFO: INSIDE Backend Helpers %j', backendHelpers);
    
    describe.only('when instantiating the module', function() {
        
        it('should be successful', function (done) {
            should.exist(backendHelpers);
            done();
        });
        
        it('should have Config Server Fixtures as an object', function(done) {
            //-- obj = {} versus undefined as a function
            obj = configServerFixtures;
            should(typeof configServerFixtures).eql('function');
            done();
        });
        
        it('should have ConfigHelper as an object', function(done) {
            //-- obj = {} versus undefined as a function
            obj = backendHelpers.configHelper();
            should(typeof obj).eql('object');
            done();
        });
        
        it('should have DataGenerationHelper as an object', function(done) {
            var obj = backendHelpers.dataGenerationHelper();
            should(typeof obj).eql('object');
            done();
        });
        
        it('should have EnvironmentHelper as an object', function(done) {
            var obj = backendHelpers.environmentHelper();
            should(typeof obj).eql('object');
            done();
        });
        
        it('should have HttpHelper as an object', function(done) {
            var obj = backendHelpers.httpHelper();
            should(typeof obj).eql('object');
            done();
        });
        
        it('should have JsonHelper as an object', function(done) {
            var obj = backendHelpers.jsonHelper();
            should(typeof obj).eql('object');
            done();
        });
        
        it('should have Loggers as an object', function(done) {
            var obj = backendHelpers.loggers();
            should(typeof obj).eql('object');
            done();
        });
        
        it('should have Loggers.Log4jsHelper as a function', function(done) {
            var obj = backendHelpers.loggers().log4jsHelper;
            should(typeof obj).eql('function');
            done();
        });
        
        it.skip('should have MsSQLClientHelper as an object', function(done) {
            var obj = backendHelpers.mongoClientHelper();
            should(typeof obj).eql('object');
            done();
        });
        
        it('should have ParameterHelper as an object', function(done) {
            var obj = backendHelpers.parameterHelper();
            should(typeof obj).eql('object');
            done();
        });
        
        it('should have Validators as an object', function(done) {
            var obj = backendHelpers.validators();
            should(typeof obj).eql('object');
            done();
        });
        
        it('should be able to connect to MS-SQL Server', function(done) {
            
            var url = 'mssql://ipAdd:portNo/dbName';
            var MsSQLClient = require('mssql').Client;
            var error = null, retDb = null;
            
            MsSQLClient.connect(url, {}, function (err, db) {
                if (err) {
                    error = err;
                    console.log('\t\t\tConnection FAIL. Callback with error\t= %j', error);
                    console.log('==============================================================');
                    done();
                }
                else {
                    retDb = db;
                    console.log('\t\t\tConnection SUCCESS. Callback with db\t= ', db);
                    console.log('==============================================================');
                    done();
                }
            });
            should(error).eql(null);
            should(typeof retDb !== null).eql(true);
            
        });
        
    });
    
    
});